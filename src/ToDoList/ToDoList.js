import React, { useEffect, useState } from "react";
import { toDoListService } from "../services/toDoListService";
import moment from "moment";
import "./toDoList.css";
import { useTranslation } from "react-i18next";
import MultiLanguage from "../component/MultiLanguage/MultiLanguage";
import { Link } from "react-router-dom";
import ListTaskInProgess from "../component/ListTask/ListTaskInProgess";
import ListTaskCompleted from "../component/ListTask/ListTaskCompleted";
import AddOrEdit from "../component/Modal/AddOrEdit";
import Search from "../component/Search/Search";

export default function ToDoList(props) {
  const { t } = useTranslation();

  const [listTask, setListTask] = useState([]);
  const [nameTask, setNameTask] = useState("");
  const [inputSearch, setInputSearch] = useState("");
  const [check, setCheck] = useState(false);
  const [filterDate, setFilterDate] = useState(moment().format("YYYY-MM-DD"));

  let handleSetNameTask = (task) => {
    setNameTask(task);
  };

  let handleSetInputSearch = (keyWord) => {
    setInputSearch(keyWord);
  };

  const handleFilter = () => {
    setListTask([
      ...listTask.filter(
        (task) =>
          moment(task.createAt).format("MM-DD-YYYY") ===
          moment(filterDate).format("MM-DD-YYYY")
      ),
    ]);
  };

  let addTask = (task) => {
    toDoListService
      .addTask(task)
      .then(() => {
        setCheck(!check);
        handleSetNameTask("");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deleteTask = (id) => {
    toDoListService
      .deleteTask(id)
      .then(() => {
        setCheck(!check);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const updateTask = (task) => {
    toDoListService
      .updateTask(task)
      .then(() => {
        setCheck(!check);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    toDoListService
      .getListTask()
      .then((res) => {
        setListTask(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [check]);

  return (
    <div className="w-[500px] p-4 bg-blue-100 rounded-md relative">
      {/* Change languages */}
      <MultiLanguage />

      {/* Add the task */}
      <div className="flex items-center justify-between">
        <button
          className="button_green"
          data-bs-toggle="modal"
          data-bs-target="#addtask"
          onClick={() => {
            handleSetNameTask("");
          }}
        >
          {t("Add task")}
        </button>
        <AddOrEdit
          nameTask={nameTask}
          handleTask={addTask}
          handleSetNameTask={handleSetNameTask}
          title="Add task"
          idModal="addtask"
        />
      </div>

      {/* Search tasks by name */}
      <Search
        inputSearch={inputSearch}
        handleSetInputSearch={handleSetInputSearch}
      />

      <div className="my-4 flex justify-between items-center">
        <label className="bold">{t("Filter by date")}: </label>
        <input
          type="date"
          id="filterDate"
          name="filterDate"
          className="text-base px-3 py-2 rounded-md"
          min="1900-01-01"
          max="2100-12-31"
          value={filterDate}
          onChange={(e) => setFilterDate(e.target.value)}
        />
        <button
          className="button_blue"
          onClick={() => {
            handleFilter();
          }}
        >
          {t("Filter")}
        </button>
        <button
          className="button_red"
          onClick={() => {
            setCheck(!check);
          }}
        >
          {t("Cancel")}
        </button>
      </div>

      {/* Tabs the list tasks */}
      <div>
        <ul className="flex space-x-2 text-xl">
          <li className="w-1/2 rounded-t-lg">
            <Link to="/inprogess">
              {props.path === "inprogess" ? (
                <span className="tab_span_active">{t("In progess")}</span>
              ) : (
                <span className="tab_span">{t("In progess")}</span>
              )}
            </Link>
          </li>
          <li className="w-1/2">
            <Link to="/completed">
              {props.path === "completed" ? (
                <span className="tab_span_active">{t("Completed")}</span>
              ) : (
                <span className="tab_span">{t("Completed")}</span>
              )}
            </Link>
          </li>
        </ul>
        <div>
          {props.path === "inprogess" ? (
            <ListTaskInProgess
              listTask={listTask}
              inputSearch={inputSearch}
              updateTask={updateTask}
              deleteTask={deleteTask}
              nameTask={nameTask}
              handleSetNameTask={handleSetNameTask}
            />
          ) : (
            <ListTaskCompleted
              listTask={listTask}
              inputSearch={inputSearch}
              updateTask={updateTask}
              deleteTask={deleteTask}
              nameTask={nameTask}
              handleSetNameTask={handleSetNameTask}
            />
          )}
        </div>
      </div>
    </div>
  );
}
