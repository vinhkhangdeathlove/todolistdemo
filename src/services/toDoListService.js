import { axiosInstance } from "../Api/axiosInstance";

export const toDoListService = {
  getListTask() {
    return axiosInstance.get("ToDoList");
  },

  addTask(task) {
    return axiosInstance.post("ToDoList", task);
  },

  updateTask(task) {
    return axiosInstance.put(`ToDoList/${task.id}`, task);
  },

  getTaskById(id) {
    return axiosInstance.get(`ToDoList/${id}`);
  },

  deleteTask(id) {
    return axiosInstance.delete(`ToDoList/${id}`);
  },
};
