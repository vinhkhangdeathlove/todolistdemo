import { useRef } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import ToDoList from "./ToDoList/ToDoList";

function App() {
  const ref = useRef();
  return (
    <div className="flex justify-center mt-10">
      <BrowserRouter>
        <Routes>
          <Route path="/inprogess" element={<ToDoList path="inprogess" />} />
          <Route path="/completed" element={<ToDoList path="completed" />} />
          <Route path="/" element={<ToDoList path="inprogess" />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
