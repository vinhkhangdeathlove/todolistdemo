import axios from "axios";

export const axiosInstance = axios.create({
  baseURL: "https://62c3da1eabea8c085a649421.mockapi.io/",
  headers: {
    "Content-Type": "application/json",
  },
});
