import { t } from "i18next";
import React, { useState } from "react";

export default function Selection(props) {
  let { data } = props;
  let [isOpen, setIsOpen] = useState(false);

  let value = window.location.href.slice(-2);
  let currentLanguage = data.find((item) => item.value === value);

  return (
    <div className="select">
      <input
        className="bg-white rounded-md flex justify-between items-center px-3 py-1.5 mb-1 border border-gray-300 focus:outline-none shadow-sm w-[150px]"
        onFocus={(e) => {
          setIsOpen(true);
        }}
        onBlur={(e) => {
          setIsOpen(false);
        }}
        value={t(currentLanguage?.text || "Language")}
      />
      {isOpen ? (
        <div className="bg-white rounded-md py-1 border border-gray-300 shadow-sm">
          {data.map((item, index) => {
            return (
              <div
                key={index}
                className="hover:bg-blue-200 flex px-3 py-1.5"
                onMouseDown={(e) => e.preventDefault()}
                onClick={(e) => {
                  setIsOpen(false);
                  window.location.replace(`?lng=${item.value}`);
                }}
              >
                <span className="uppercase w-4">{item.value}</span>
                <span className="ml-4">{t(item.text)}</span>
              </div>
            );
          })}
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}
