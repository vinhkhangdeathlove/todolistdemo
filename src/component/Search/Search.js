import { t } from "i18next";
import React from "react";

export default function Search(props) {
  let { inputSearch, handleSetInputSearch } = props;
  return (
    <div className="relative">
      <input
        type="text"
        className="form-control w-full pl-10 py-1.5 text-base font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-md
        transition m-0 focus:outline-none mt-4"
        id="taskInput"
        value={inputSearch}
        placeholder={t("Search")}
        onChange={(e) => handleSetInputSearch(e.target.value)}
      />
      <div className="absolute left-4 top-6">
        <i className="fa fa-search" />
      </div>
    </div>
  );
}
