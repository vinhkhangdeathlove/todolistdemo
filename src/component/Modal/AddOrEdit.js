import moment from "moment";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";

export default function AddOrEdit(props) {
  let { title, nameTask, handleTask, handleSetNameTask, idTask, idModal } =
    props;
  const { t } = useTranslation();
  const [date, setDate] = useState(moment().format("YYYY-MM-DD"));

  return (
    <>
      <div
        className="modal fade fixed top-0 left-0 hidden w-full h-full overflow-x-hidden overflow-y-auto"
        id={idModal}
        tabIndex={-1}
        aria-hidden="true"
      >
        <div className="modal-dialog relative">
          <div className="modal-content relative flex flex-col w-full bg-white rounded-md">
            <div className="relative p-4">
              <div className="flex justify-between mb-2">
                <span className="font-semibold text-green-500 text-xl">
                  {t(title)}
                </span>
                <button
                  type="button"
                  className="btn-close w-4 h-4 text-black hover:text-black hover:opacity-75"
                  data-bs-dismiss="modal"
                />
              </div>

              <div className="bg-blue-200 p-4 border-2 rounded-md">
                <input
                  type="text"
                  className="form-control w-full px-3 py-2 text-base font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-md transition focus:outline-none"
                  id="taskInput"
                  value={nameTask}
                  placeholder={t("Task")}
                  onChange={(e) => handleSetNameTask(e.target.value)}
                />
                <div>
                  <label className="bold">{t("Select date")}: </label>
                  <input
                    type="date"
                    id="date"
                    name="date"
                    className="text-base mt-4 px-3 py-2 rounded-md"
                    min="1900-01-01"
                    max="2100-12-31"
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                  />
                </div>
                <div className="mt-4 flex justify-end space-x-4">
                  <button
                    type="button"
                    className="button_red"
                    data-bs-dismiss="modal"
                  >
                    {t("Cancel")}
                  </button>
                  <button
                    type="button"
                    className="button_blue"
                    onClick={() => {
                      handleTask({
                        id: idTask ? idTask : "",
                        nameTask: nameTask,
                        createAt: date,
                      });
                      console.log(idTask);
                      handleSetNameTask("");
                    }}
                    data-bs-dismiss="modal"
                  >
                    {t("Confirm")}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
