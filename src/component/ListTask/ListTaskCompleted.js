import moment from "moment";
import React, { useState } from "react";
import AddOrEdit from "../Modal/AddOrEdit";

export default function ListTaskInProgess(props) {
  let {
    listTask,
    inputSearch,
    updateTask,
    deleteTask,
    nameTask,
    handleSetNameTask,
  } = props;
  const [idTask, setIdTask] = useState(0);
  return (
    <ul className="bg-white p-4 space-y-2 rounded-b-lg">
      {listTask
        .filter((task) =>
          task.nameTask.toLowerCase().includes(inputSearch.toLowerCase())
        )
        .map((task) => {
          if (task.isCompleted)
            return (
              <li key={task.id} className="item_task">
                <div>
                  <span className="mr-4">
                    {moment(task.createAt).format("MM-DD-YYYY")}
                  </span>
                  <span>{task.nameTask}</span>
                </div>
                <div className="flex space-x-2">
                  <button
                    onClick={() => {
                      updateTask({
                        id: task.id,
                        nameTask: task.nameTask,
                        isCompleted: !task.isCompleted,
                      });
                    }}
                  >
                    <img
                      className="icon-check-on w-4 h-4"
                      src="https://frontend.tikicdn.com/_desktop-next/static/img/pdp_revamp_v2/checked.svg"
                      alt=""
                    />
                  </button>
                  <button
                    data-bs-toggle="modal"
                    data-bs-target="#edittask"
                    onClick={() => {
                      setIdTask(task.id);
                      handleSetNameTask(task.nameTask);
                    }}
                  >
                    <i className="fa fa-edit text-green-500" />
                  </button>
                  <AddOrEdit
                    nameTask={nameTask}
                    idTask={idTask}
                    handleTask={updateTask}
                    handleSetNameTask={handleSetNameTask}
                    title="Edit Task"
                    idModal="edittask"
                  />
                  <button
                    onClick={() => {
                      deleteTask(task.id);
                    }}
                  >
                    <i className="fa fa-trash-alt text-red-500" />
                  </button>
                </div>
              </li>
            );
        })}
    </ul>
  );
}
