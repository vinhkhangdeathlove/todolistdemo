import React from "react";
import Selection from "../Selection/Selection";

export default function MultiLanguage() {
  const languages = [
    { value: "en", text: "English" },
    { value: "fr", text: "French" },
    { value: "vi", text: "Vietnamese" },
  ];

  return (
    <div className="absolute right-4 top-4">
      <Selection data={languages} />
    </div>
  );
}
