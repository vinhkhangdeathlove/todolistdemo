import { useEffect, useState } from "react";
import { toDoListService } from "../../services/toDoListService";

export default function useFetch(url) {
  let [listTask, setListTask] = useState([]);

  useEffect(() => {
    toDoListService
      .getListTask()
      .then((res) => {
        setListTask(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return [listTask];
}
