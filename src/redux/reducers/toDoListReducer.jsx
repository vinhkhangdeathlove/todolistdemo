import { GET_LIST_TASK } from "../constants/toDoListConstant";

let initialState = {
  listTask: [],
};

export const toDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_TASK: {
      return {
        ...state,
        listTask: action.payload,
      };
    }

    default:
      return state;
  }
};
